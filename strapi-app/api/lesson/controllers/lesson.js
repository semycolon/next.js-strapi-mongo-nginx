'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/v3.x/concepts/controllers.html#core-controllers)
 * to customize this controller
 */

module.exports = {
  by_category: async function(ctx){
    console.log(ctx);
    //todo filter the count of lessons
    return (await strapi.services.lesson.by_category());
  }
};
